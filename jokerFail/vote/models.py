from django.db import models

# Create your models here.
class Joker(models.Model):
    name = models.CharField(primary_key=True, max_length=50)
    score = models.IntegerField()
    image = models.FileField(upload_to = 'images/', default = 'images/not-img.jpg')
    def __str__(self):
        return self.name
