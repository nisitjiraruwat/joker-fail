from django.shortcuts import render,Http404
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.files import File
from django.conf import settings

import datetime
import json

from .models import Joker

# Create your views here.

_jokers = {}

""" check list time insert """
def check_joker_vote(jokers, joker_vote):
    count_ready = 0
    check_del = False
    mem_keys = []
    mem_keys_overfow = []
    for key, value in jokers.items():
        if jokers[key]['to'] == joker_vote['to'] and joker_vote['time'] - jokers[key]['time'] <= 5.0:
            count_ready += 1
            mem_keys.append(key)
            if count_ready == 3:
                del_joker_vote(jokers, mem_keys)
                return add_score(joker_vote['to'])
        elif joker_vote['time'] - jokers[key]['time'] > 10.0:
            mem_keys_overfow.append(key)

    del_joker_vote(jokers, mem_keys_overfow)
    return 0

def del_joker_vote(jokers, mem_keys):
    for key in mem_keys:
        del jokers[key]

def add_score(name):
    joker = Joker.objects.get(name = name)
    joker.score = joker.score + 1;
    joker.save()
    return joker.score

def rename_img(old_name, new_name):
    split_name = old_name.split('.')
    last = len(split_name) - 1
    return new_name + '.'+ split_name[last]

def save_img(img):
    with open(settings.MEDIA_ROOT + 'images/' + img.name, 'wb+') as destination:
        for chunk in img.chunks():
            destination.write(chunk)

def get_super_joker():
    super_jokers = Joker.objects.filter(score__gte = 1).order_by('-score')
    if len(super_jokers) > 4:
        return super_jokers[:4]
    return super_jokers;

@csrf_exempt
def vote(request):
    if request.method == "PUT":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            Joker.objects.get(name = body['from'])
            Joker.objects.get(name = body['to'])
            _jokers[body['from'] + body['to']] = {
                'to': body['to'],
                'time': datetime.datetime.now().timestamp()
            }

            result = check_joker_vote(_jokers, _jokers[body['from'] + body['to']])
            return JsonResponse({
                'to': body['to'],
                'score': result
                })
        except Exception:
            raise Http404("NOT1")
        return HttpResponse("NOT2")
    else:
        raise Http404("NOT3")

@csrf_exempt
def load(request):
    if request.method == "GET":
        try:
            super_jokers = get_super_joker()
            super_jokers_json = serializers.serialize("json", super_jokers)
            jokers_json = serializers.serialize("json", Joker.objects.all().order_by('-score')[len(super_jokers):])
            return JsonResponse({'super-jokers': super_jokers_json, 'jokers': jokers_json})
        except Exception:
            raise Http404("NOT1")
        return HttpResponse("NOT2")
    else:
        raise Http404("NOT3")

@csrf_exempt
def register(request):
    if request.method == "POST":
        try:
            img = request.FILES['img']
            img.name = rename_img(img.name, request.POST['name'])
            joker = Joker(name = request.POST['name'], score = 0, image = img)
            joker.save()
            return JsonResponse({
                'name': joker.name,
                'score': joker.score,
                'img': joker.image.name,
                })
        except Exception:
            raise Http404("NOT1")
        return HttpResponse("NOT2")
    else:
        raise Http404("NOT3")

@csrf_exempt
def login(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            joker = Joker.objects.get(name = body['name'])
            return JsonResponse({
                'name': joker.name
                })
        except Joker.DoesNotExist:
            raise Http404("Joker does not exist")
    else:
        raise Http404("NOT")

@csrf_exempt
def changeimg(request):
    if request.method == "POST":
        try:
            joker = Joker.objects.get(name = request.POST['name'])
            img = request.FILES['img']
            img.name = rename_img(img.name, joker.name)
            joker.image.name = 'images/' + img.name
            save_img(img)
            joker.save()

            return JsonResponse({
                'img': joker.image.name
                })
        except Joker.DoesNotExist:
            raise Http404("Joker does not exist")
    else:
        raise Http404("NOT")
