# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vote', '0002_auto_20151110_0656'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='joker',
            name='id',
        ),
        migrations.AlterField(
            model_name='joker',
            name='name',
            field=models.CharField(serialize=False, primary_key=True, max_length=50),
        ),
    ]
