from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.load, name='load'),
    url(r'^vote/$', views.vote, name='vote'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.login, name='login'),
    url(r'^changeimg/$', views.changeimg, name='changeimg'),
]
