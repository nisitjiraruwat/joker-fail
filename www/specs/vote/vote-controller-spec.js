describe('vote-controller', function() {

    var ctrl,
        Vote ,
        $httpBackend,
        $q,
        $rootScope;

    beforeEach(module('jokerFailApp'));

    beforeEach(inject(function(_Vote_, $controller, _$httpBackend_, _$q_, _$rootScope_) {

        ctrl = $controller('voteController');
        Vote = _Vote_;
        $httpBackend = _$httpBackend_;
        $q = _$q_;
        $rootScope = _$rootScope_;
    }));

   it('should vote success', function() {

       var voteFrom = 'ben';
       var voteTo = 'vee';

       spyOn(Vote, 'vote').and.returnValue($q.when({}));
       ctrl.vote(voteFrom, voteTo);
       $rootScope.$apply();
       expect(Vote.vote).toHaveBeenCalledWith(voteFrom, voteTo);
       expect(ctrl.value).toEqual({});

    });

 });
