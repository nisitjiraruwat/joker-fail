let registerController = ['$log', 'Register', '$state',
    function($log, Register, $state){

        $log.log('[registerController] Start.');

        let ctrl = this;

        ctrl.from = false;
        ctrl.name = '';

        ctrl.jeckers = {};
        ctrl.img = {};
        ctrl.value = '';

        ctrl.register = (file, name) => {
            if(!file.name || name === '') {
                return;
            }

            return Register.register(file, name)
                .then((response) => {
                    $log.info('vote result: ', response);
                    ctrl.value = response;
                    $state.go('otherwise');
                    return response;
                });
        };

        $log.log('[registerController] End.');

    }];

export {
    registerController
}
