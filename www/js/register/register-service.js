let Register =  ['$log', '$rootScope', '$http',
    function($log, $rootScope, $http) {

        $log.log('[Register] Start.');

        let register,
            getFormData;

        getFormData = (file, name) => {
            let fd = new FormData();
            fd.append('name', name);
            fd.append('img', file);
            return fd;
        };

        register = (file, name) => {
            let data = getFormData(file, name);
            //.post('http://localhost:8082/vote/register/', {name: 'ben'}
            return $http({
                    method  : 'POST',
                    url     : 'vote/register/',
                    transformRequest: angular.identity,
                    data    : data,
                    headers: {'Content-Type': undefined}
                 })
                .then((response) => {
                    return response.data;
                });
        };

        $log.log('[Register] End.');

        return {
            register
        };

    }];

export {
    Register
}
