import angular from 'angular';

import 'angular-material';
import 'angular-ui-router';

import './login/module';
import './vote/module';
import './register/module';

let jokerFailApp = angular.module('jokerFailApp', [
    'ngMaterial',
    'ui.router',
    'login',
    'vote',
    'register'
]);

jokerFailApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(e){
                scope.$apply(function(){
                    modelSetter(scope, e.target.files[0]);
                });
            });
        }
    };
}]);

import {updateFileModel} from './update-file-model';
jokerFailApp.directive('updateFileModel', updateFileModel);

jokerFailApp.config(['$urlRouterProvider', function($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
}]);

jokerFailApp.config(['$stateProvider', ($stateProvider) => {
    $stateProvider
        .state('otherwise', {
            url: '/',
            templateUrl: 'login.html',
            controller: "loginController as ctrl"
        })
        .state('vote', {
            url: '/vote',
            templateUrl: 'vote.html',
            controller: "voteController as ctrl"
        }).state('register', {
            url: '/register',
            templateUrl: 'register.html',
            controller: "registerController as ctrl"
        });

}]);

jokerFailApp.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider
        .iconSet('action', './../icons/svg-sprite-action.svg')
        .iconSet('alert', './../icons/svg-sprite-alert.svg')
        .iconSet('av', './../icons/svg-sprite-av.svg')
        .iconSet('content', './../icons/svg-sprite-content.svg')
        .iconSet('editor', './../icons/svg-sprite-editor.svg')
        .iconSet('file', './../icons/svg-sprite-file.svg')
        .iconSet('image', './../icons/svg-sprite-image.svg')
        .iconSet('maps', './../icons/svg-sprite-maps.svg')
        .iconSet('navigation', './../icons/svg-sprite-navigation.svg')
        .iconSet('toggle', './../icons/svg-sprite-toggle.svg');
}]);
