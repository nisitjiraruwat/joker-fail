import angular from 'angular';

import 'angular-cookies';

let vote = angular.module('vote', ['ngCookies']);

import {Vote} from './vote-service';
vote.factory('Vote', Vote);

import {voteController} from './vote-controller';
vote.controller('voteController', voteController);
