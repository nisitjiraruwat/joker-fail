# *Set Django* 

    MEDIA_ROOT = '/home/var/myproject/joker-fail/media/'

# *Python3* 

    pip3 
        - Django (1.8.6)
        - django-cors-headers (1.1.0)
        - pip (1.5.4)
        - setuptools (2.2)


# *Set nginx* 

    server {
        listen 8081;
    
       location / {
            alias /home/nisit/myproject/joker-fail/www/dist/;
            try_files $uri $uri/ /index.html;
       }

       location /admin/ {
            proxy_pass http://192.168.1.100:8082/admin/;
       }

       location /vote/ {
            proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_redirect off;
            proxy_pass  http://192.168.1.100:8082/vote/;
       }


       location /static/ {
            autoindex on;
            alias /home/nisit/myproject/joker-fail/static/;
       }

       location /media/ {
            autoindex on;
            alias /home/nisit/myproject/joker-fail/media/;
       }
    }